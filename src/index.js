import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import MainPage from './components/MainPage';
import Payment from './components/Payment';
import BoardPage from './components/BoardPage';
import MarketPage from './components/MarketPage';
import GamePage from './components/GamePage';
import './styles/index.scss';
import './font.scss';
import './preprocessing/normalize.scss';


const renderMainPage = (props) => {
    return (
        <MainPage {...props}/>
    );
}
const renderPayment = (props) => {
    return (
        <Payment {...props}/>
    );
}
const renderBoardPage = (props) => {
    return (
        <BoardPage {...props}/>
    );
}
const renderMarketPage = (props) => {
    return (
        <MarketPage {...props}/>
    );
}
const renderGamePage = (props) => {
    return (
        <GamePage {...props}/>
    );
}
const App = (props) => {
    return (
        <Router>
            <Route path='/payment' render={(props) => renderPayment(props)} />
            <Route path='/game' render={(props) => renderGamePage(props)} />
            <Route path='/' exact={true} render={(props) => renderMainPage(props)} />
            <Route path='/board' exact={true} render={(props) => renderBoardPage(props)} />
            <Route path='/market' exact={true} render={(props) => renderMarketPage(props)} />
        </Router>
    );
}
ReactDOM.render(<App />, document.getElementById('root'));

