import {Cookies} from 'react-cookie';

export const baseURL = 'https://api.phosphor.tika-team.ir/';
export const cookies = new Cookies();