import React from 'react';
import './styles/BoardPageXl.scss';
import {Link} from 'react-router-dom';
import persianJs from 'persianjs';
import BounceLoader from 'react-spinners/BounceLoader';

const renderLoader = (props) => {
    if (props.loading) {
        return (
            <div className="board-page-loader-container-xl">
                <BounceLoader
                    sizeUnit={"px"}
                    size={150}
                    className="board-page-loader-xl"
                    color={'#66a4ff'}
                    loading={props.loading}/>
            </div>
        );
    }
}

const renderList = (props) => {
    if (!props.loading) {
        return (
            <div className="board-page-outer-container-xl">
                <div className="board-page-header">
                    <Link to={'/'} >
                            <img src={require('./assets/back.png')} />
                    </Link>
                </div>
                <div className="top-board">
                    {props.list.length >= 1?
                        <div className="board-page-ranks">
                            <div className="board-page-ranking1"></div>
                            <p>{props.list[0].fullname}</p>
                            <h3>{persianJs(props.list[0].score.toString()).englishNumber().toString()}</h3>
                        </div>
                    :""}
                    {props.list.length >= 2?
                        <div className="board-page-ranks">
                            <div className="board-page-ranking2"></div>
                            <p>{props.list[1].fullname}</p>
                            <h3>{persianJs(props.list[1].score.toString()).englishNumber().toString()}</h3>
                        </div>
                    :""}
                    {props.list.length >= 3?
                        <div className="board-page-ranks">
                            <div className="board-page-ranking3"></div>
                            <p>{props.list[2].fullname}</p>
                            <h3>{persianJs(props.list[2].score.toString()).englishNumber().toString()}</h3>
                        </div>
                    :""}
                </div>
                <div className="board-page-parent-xl">
                    <div className="board-page-board">
                        <p>{persianJs(props.profile.rank.toString()).englishNumber().toString()}</p>
                        <h2>{props.profile.fullname}</h2>
                        <h3>{persianJs(props.profile.score.toString()).englishNumber().toString()}</h3>
                    </div>
                    <div className="board-page-board-container-xl">
                        {props.list.map((item, index) => 
                            <div key={index} className="board-page-board-item-container-xl">
                                <p className="board-page-board-item-score-xl">
                                    {persianJs(item.score).englishNumber().toString()}
                                </p>
                                <p className="board-page-board-item-name-xl">
                                    {item.fullname}
                                </p>                        
                                <p className="board-page-board-item-rank-xl">
                                    {persianJs(item.rank).englishNumber().toString()}
                                </p>
                            </div>    
                        )}
                    </div>
                </div>
            </div>
        );
    }
}
const BoardPageXl = (props) => {
    return (
        <div>
            {renderLoader(props)}
            {renderList(props)}
        </div>
    );
}

export default BoardPageXl;