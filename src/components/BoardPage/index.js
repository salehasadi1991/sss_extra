import React, {lazy, Suspense, useState, useEffect} from 'react';
import {getLeaderBoardList} from './network/board';
import MediaQuery from 'react-responsive';

const BoardPageXl = lazy(() => import('./BoardPageXl'));
const BoardPageXs = lazy(() => import('./BoardPageXs'));
const BoardPageSm = lazy(() => import('./BoardPageSm'));


const BoardPage = (props) => {
    const [list, setList] = useState([]);
    const [loading, setLoading] = useState(true);
    const [profile, setProfile] = useState([]);


    const onMessage = (event) => {
        const data = JSON.parse(event.data);
        if (data.type === 'ACTION_BACK') {
            props.history.replace('/');
        }
    }
    useEffect(() => {
        document.addEventListener("message", onMessage ,false);
        return () => {
            document.removeEventListener("message", onMessage ,false);
        }
    }, []);

    
    useEffect(() => {

        getLeaderBoardList((response) => {  
            setList(response.leaderboard);
            setProfile(response.profile);
            setLoading(false);
        });
    }, []);
    return (
        <React.Fragment>
            <MediaQuery minWidth={320} maxWidth={480}>
                <Suspense fallback={<div></div>}>
                    <BoardPageXs
                        list={list}
                        profile={profile}
                        loading={loading} 
                        />
                </Suspense>
            </MediaQuery>
            <MediaQuery minWidth={481} maxWidth={768}>
                <Suspense fallback={<div></div>}>
                    <BoardPageSm 
                        list={list}
                        profile={profile}
                        loading={loading} />  
                </Suspense>
            </MediaQuery>    
            <MediaQuery minWidth={769}>
                <Suspense fallback={<div></div>}>
                    <BoardPageXl 
                        list={list}
                        profile={profile}
                        loading={loading} />
                </Suspense>
            </MediaQuery>
        </React.Fragment>
    );
}

export default BoardPage;