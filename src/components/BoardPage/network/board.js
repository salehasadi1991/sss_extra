import axios from 'axios';
import {baseURL, cookies} from '../../../Global.js';


export function getLeaderBoardList(callback) {
    axios({
        url: baseURL + 'v1/usr/leaderboard/',
        method:'get',
        headers: {
            'Authorization': 'Token ' + cookies.get('token'), 
        },
    })
    .then(response => {
        callback(response.data);
    })
    .catch(error => {
        callback(error.response.data)
    });
}