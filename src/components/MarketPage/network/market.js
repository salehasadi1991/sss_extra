import axios from 'axios';
import {baseURL, cookies} from '../../../Global.js';

export  function getSku(callback) {
    axios({
        url: baseURL + 'v1/fin/sku/',
        method:'get',
        headers: {
            'Authorization': 'TOKEN ' + cookies.get('token'), 
        },
    })
    .then(response => {
        callback(response);
    })
    .catch(error => {
        callback(error.response);
    });
}
export function setPay(skuId, callback) {
    axios({
        url: baseURL + 'v1/fin/pay/',
        method:'post',
        headers: {
            'Authorization': 'TOKEN ' + cookies.get('token'), 
        },
        data: {
          "sku_id": skuId,
          "channel": "gplay"
        },
    })
    .then(response => {
        callback(response.data);
    })
    .catch(error => {
        callback(error.response.data)
    });
}
