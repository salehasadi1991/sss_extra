import React from 'react';
import BounceLoader from 'react-spinners/BounceLoader';
import persianJs from 'persianjs';
import {setPay} from './network/market';
import {Link} from 'react-router-dom';
import './styles/MarketPageXs.scss';

const renderLoader = (props) => {
    if (props.loading) {
        return (
            <div className="market-page-loader-container-xs">
                <BounceLoader
                    sizeUnit={"px"}
                    size={150}
                    className="market-page-loader-xs"
                    color={'#66a4ff'}
                    loading={props.loading}/>
            </div>
        );
    }
}

const renderCoinList = (props) => {
    if (!props.loading) {
        return (
            <div className="market-page-outer-container-xs">
                <div className="market-page-header">
                    <Link to={'/'} >
                            <img src={require('./assets/back.png')} />
                    </Link>
                </div>
                <div className="market-page-list-container-xs">
                    {props.sku.map((item, index) => 
                        <div 
                            key={index} 
                            className="market-page-list-item-container-xs"
                            onClick={()=> {setPay(item.id,(response) => {
                                if(response.detail === "succeed"){
                                    window.location.replace(response.url);
                                }
                            });props.setLoading(true)}}
                        >
                            <img src={require('./assets/4.png')} />
                            <p className="market-page-list-item-title-xs">
                                {persianJs(item.title).englishNumber().toString()}
                            </p>
                            <p className="market-page-list-item-price-xs">
                                {persianJs(item.price).englishNumber().toString()} <span>تومان</span>
                            </p>
                        </div>        
                    )}
                </div>
            </div>
        );
    }
}
const MarketPageXs = (props) => {
    return (
        <div>
            {renderLoader(props)}
            {renderCoinList(props)}
        </div>
    );
}
export default MarketPageXs;