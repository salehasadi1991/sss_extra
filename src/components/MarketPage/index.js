import React, {lazy, Suspense, useState, useEffect} from 'react';
import {getSku} from './network/market';
import MediaQuery from 'react-responsive';

const MarketPageXl = lazy(() => import('./MarketPageXl'));
const MarketPageXs = lazy(() => import('./MarketPageXs'));
const MarketPageSm = lazy(() => import('./MarketPageSm'));


const MarketPage = (props) => {
    const [loading, setLoading] = useState(true);
    const [sku, setSku] = useState([]);

    const onMessage = (event) => {
        const data = JSON.parse(event.data);
        if (data.type === 'ACTION_BACK') {
            props.history.replace('/');
        }
    }
    useEffect(() => {
        document.addEventListener("message", onMessage ,false);
        return () => {
            document.removeEventListener("message", onMessage ,false);
        }
    }, []);

    useEffect(() => {  
        getSku((response) => {
            setSku(response.data);
            setLoading(false)
        });    
    },[]);
    return (
        <React.Fragment>
            <MediaQuery minWidth={320} maxWidth={480}>
                <Suspense fallback={<div></div>}>
                    <MarketPageXs 
                        loading={loading}
                        setLoading={setLoading}
                        sku={sku}/>
                </Suspense>
            </MediaQuery>
            <MediaQuery minWidth={481} maxWidth={768}>
                <Suspense fallback={<div></div>}>
                    <MarketPageSm 
                        loading={loading}
                        setLoading={setLoading}
                        sku={sku}/>  
                </Suspense>
            </MediaQuery>    
            <MediaQuery minWidth={769}>
                <Suspense fallback={<div></div>}>
                    <MarketPageXl 
                        loading={loading}
                        setLoading={setLoading}
                        sku={sku}/>
                </Suspense>
            </MediaQuery>
        </React.Fragment>
    );
}

export default MarketPage;