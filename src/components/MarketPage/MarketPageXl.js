import React from 'react';
import './styles/MarketPageXl.scss';
import {Link} from 'react-router-dom';
import {setPay} from './network/market';
import BounceLoader from 'react-spinners/BounceLoader';
import persianJs from 'persianjs';


const renderLoader = (props) => {
    if (props.loading) {
        return (
            <div className="market-page-loader-container-xl">
                <BounceLoader
                    sizeUnit={"px"}
                    size={150}
                    className="market-page-loader-xl"
                    color={'#66a4ff'}
                    loading={props.loading}/>
            </div>
        );
    }
}

const renderCoinList = (props) => {
    if (!props.loading) {
        return (
            <div className="market-page-outer-container-xl">
                <div className="market-page-header">
                    <Link to={'/'} >
                            <img src={require('./assets/back.png')} />
                    </Link>
                </div>
                <div className="market-page-list-container-xl">
                    {props.sku.map((item, index) => 
                        <div 
                            key={index} 
                            className="market-page-list-item-container-xl"
                            onClick={()=> {setPay(item.id,(response) => {
                                if(response.detail === "succeed"){
                                    window.location.replace(response.url);
                                }
                            });props.setLoading(true)}}
                        >
                            <img src={require('./assets/4.png')} />
                            <p className="market-page-list-item-title-xl">
                                {persianJs(item.title).englishNumber().toString()}
                            </p>
                            <p className="market-page-list-item-price-xl">
                                {persianJs(item.price).englishNumber().toString()} <span>تومان</span>
                            </p>
                        </div>        
                    )}
                </div>
            </div>
        );
    }
}
const MarketPageXl = (props) => {
    return (
        <div>
            {renderLoader(props)}
            {renderCoinList(props)}
        </div>
    );
}

export default MarketPageXl;