import React, {lazy, Suspense,useEffect,useState} from 'react';
import MediaQuery from 'react-responsive';
import {setSettle} from './network/payment';

const PaymentXl = lazy(() => import('./PaymentXl'));
const PaymentXs = lazy(() => import('./PaymentXs'));
const PaymentSm = lazy(() => import('./PaymentSm'));


const Payment = (props) => {
    const [title, setTitle] = useState([]);
    let query = window.location;
    var url = new URL(query);
    let status = url.searchParams.get("status");
    let pay_token = url.searchParams.get("token");
    const onMessage = (event) => {
        const data = JSON.parse(event.data);
        if (data.type === 'ACTION_BACK') {
            props.history.replace('/');
        }
    }
    useEffect(() => {
        document.addEventListener("message", onMessage ,false);
        return () => {
            document.removeEventListener("message", onMessage ,false);
        }
    }, []);
    useEffect(() => {
        if(status === "succeed"){
            setSettle(pay_token, (response) => {});
            setTitle("پرداخت شما با موفقیت انجام شد");
        }else if(status === "failed"){
            setTitle("پرداخت شما ناموفق بود");
        }
    },[]);
    return (
        <React.Fragment>
            <MediaQuery minWidth={320} maxWidth={480}>
                <Suspense fallback={<div></div>}>
                    <PaymentXs 
                        title={title}
                    />
                </Suspense>
            </MediaQuery>
            <MediaQuery minWidth={481} maxWidth={768}>
                <Suspense fallback={<div></div>}>
                    <PaymentSm 
                        title={title}
                    />  
                </Suspense>
            </MediaQuery>    
            <MediaQuery minWidth={769}>
                <Suspense fallback={<div></div>}>
                    <PaymentXl 
                        title={title}
                    />
                </Suspense>
            </MediaQuery>
        </React.Fragment>
    );
}

export default Payment;