import React from 'react';
import {Link} from 'react-router-dom';
import './styles/PaymentXl.scss';


const PaymentXl = (props) => {
    return (
        <div className="payment-container-xl">
            <h2>{props.title}</h2>
            <Link to={"/"}>بازگشت به بازی</Link>
        </div>
    );
}

export default PaymentXl;