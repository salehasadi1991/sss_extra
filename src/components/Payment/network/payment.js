import axios from 'axios';
import {baseURL, cookies} from '../../../Global.js';


export  function setSettle(pay_token,callback) {
    axios({
        url: baseURL + 'v1/fin/settle/',
        method:'post',
        data:{
            "payment_token": pay_token
        },
        headers: {
            'Authorization': 'TOKEN ' + cookies.get('token'), 
        },
    })
    .then(response => {
        callback(response);
    })
    .catch(error => {
        callback(error.response);
    });
}
