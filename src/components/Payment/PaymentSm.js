import React from 'react';
import {Link} from 'react-router-dom';
import './styles/PaymentSm.scss';


const PaymentSm = (props) => {
    return (
        <div className="payment-container-sm">
            <h2>{props.title}</h2>
            <Link to={"/"}>بازگشت به بازی</Link>
        </div>
    );
}

export default PaymentSm;