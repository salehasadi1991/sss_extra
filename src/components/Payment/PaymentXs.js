import React from 'react';
import {Link} from 'react-router-dom';
import './styles/PaymentXs.scss';


const PaymentXs = (props) => {
    return (
        <div className="payment-container-xs">
            <h2>{props.title}</h2>
            <Link to={"/"}>بازگشت به بازی</Link>
        </div>
    );
}

export default PaymentXs;