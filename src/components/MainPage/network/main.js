import axios from 'axios';
import {baseURL, cookies} from '../../../Global.js';



export function getToken(data, callback) {
    axios({
        url: baseURL + 'v1/usr/baazigooshi/token/',
        method:'post',
        data: {
          "user_token": data,
          "game_id":18
        },
        
    })
    .then(response => {
        callback(response.data);
    })
    .catch(error => {
        callback(error.response.data)
    });
}
export  function getInfo(callback) {
    axios({
        url: baseURL + 'v1/usr/info/',
        method:'get',
        headers: {
            'Authorization': 'TOKEN ' + cookies.get('token'), 
        },
    })
    .then(response => {
        callback(response);
    })
    .catch(error => {
        callback(error.response);
    });
}
export  function getHints(callback) {
    axios({
        url: baseURL + 'v1/fin/list/hint/',
        method:'get',
        headers: {
            'Authorization': 'TOKEN ' + cookies.get('token'), 
        },
    })
    .then(response => {
        callback(response);
    })
    .catch(error => {
        callback(error.response);
    });
}
export function setHint(hintId, callback) {
    axios({
        url: baseURL + 'v1/fin/use/hint/',
        method:'post',
        headers: {
            'Authorization': 'TOKEN ' + cookies.get('token'), 
        },
        data: {
          "hint_id": hintId
        },
    })
    .then(response => {
        callback(response.data);
    })
    .catch(error => {
        callback(error.response.data)
    });
}