import React from 'react';
import {Link} from 'react-router-dom';
import persianJs from 'persianjs';
import BounceLoader from 'react-spinners/BounceLoader';
import './styles/MainPageXl.scss';
import {setHint} from './network/main';
const renderLoader = (props) => {
    if (props.loading) {
        return (
            <div className="main-page-loader-container-xl">
                <BounceLoader
                    sizeUnit={"px"}
                    size={150}
                    className="main-page-loader-xl"
                    color={'#66a4ff'}
                    loading={props.loading}/>
            </div>
        );
    }
}
const renderList = (props) => {
    if(!props.loading){
        return (
            <div className="main-game-outher-container-xl">
                <div className ="main-game-container-xl">
                    <div className="main-game-show-point-xl">
                        <h2>امتیاز شما</h2>
                        <p>{persianJs(props.userInfo.max_score.toString()).englishNumber().toString()}</p>
                    </div>
                        <label className="main-page-link-xl" htmlFor="game-page-end">شروع</label>
                </div>
                <div className="main-page-icons-xl">
                    <Link to={'market/'} >
                        <img className="main-page-store-icon" src={require('./assets/images/store.png')} />
                    </Link>
                    <Link to={'board/'} >
                        <img className="main-page-leaderboard-icon" src={require('./assets/images/leaderboard.png')} />
                    </Link>
                </div>
                <input className="game-page-end-state" id="game-page-end" type="checkbox" />
                <div className="game-page-end">
                    <div className="game-page-end-inner">
                        <div className="modal-box-for">


                            {props.timeForGame === 0 ?
                                <div 
                                    className="modal-box-time"
                                    onClick={()=> setHint(props.extraTime.id,(response) => {
                                        props.setHintDetail(response);
                                        if(props.userInfo.coin >= props.extraTime.cost){
                                            props.setTimeForGame(1);
                                        }else{
                                            props.setCoinText('');
                                            props.setCoinText('سکه شما کافی نیست');
                                        }
                                    })}
                                >
                                    <img src={require('./assets/images/time.png')} alt=""/>
                                    
                                    <h3>{props.extraTime.name}</h3>
                                        
                                
                                    <p>{props.extraTime.cost}</p>
                                </div>
                            :
                                <div className="modal-box-time-checked" >
                                    <img src={require('./assets/images/time.png')} alt=""/>
                                    <h3>{props.extraTime.name}</h3>
                                    <p>{props.extraTime.cost}</p>
                                </div>
                            }
                            {props.boosterForGame === 0 ?
                                <div 
                                    className="modal-box-booster"
                                    onClick={()=> setHint(props.booster.id,(response) => {
                                        props.setHintDetail(response);
                                        if(props.userInfo.coin >= props.booster.cost){
                                            props.setBoosterForGame(1);
                                        }else{
                                            props.setCoinText('');
                                            props.setCoinText('سکه شما کافی نیست');
                                        }
                                    })}
                                >
                                    <img src={require('./assets/images/booster.png')} alt=""/>
                                    <h3>{props.booster.name}</h3>
                                    <p>{props.booster.cost}</p>
                                </div>
                                :
                                <div className="modal-box-booster-checked">
                                    <img src={require('./assets/images/booster.png')} alt=""/>
                                    <h3>{props.booster.name}</h3>
                                    <p>{props.booster.cost}</p>
                                </div>
                            }
                            <div className="coin-text">
                                <p>{props.coinText}</p>
                            </div>
                            <Link to={{ pathname: 'game/', hints:{ booster: props.boosterForGame, extra_time: props.timeForGame },max_score:props.userInfo.max_score }} >
                                <div className="modal-box-game-link-game">
                                    <p>برو تو بازی</p>
                                </div>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const MainPageXl = (props) => {
    return (
        <div>
            {renderLoader(props)}
            {renderList(props)}
        </div>
    );
}
export default MainPageXl;