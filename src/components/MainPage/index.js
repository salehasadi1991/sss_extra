import React, {lazy, Suspense, useEffect,useState} from 'react';
import MediaQuery from 'react-responsive';
import {cookies} from '../../Global.js';
import {getToken, getInfo, getHints} from './network/main';


const MainPageXl = lazy(() => import('./MainPageXl'));
const MainPageXs = lazy(() => import('./MainPageXs'));
const MainPageSm = lazy(() => import('./MainPageSm'));
const url = window.location.href.split('-')[1];


const MainPage = (props) => {
    const [userInfo, setUserInfo] = useState({});
    const [coinText, setCoinText] = useState('');
    const [loading, setLoading] = useState(true);
    const [payload , setPayLoad]= useState();
    const [booster, setBooster] = useState([]);
    const [extraTime, setExtraTime] = useState([]);
    const [hintDetail, setHintDetail] = useState([]);
    const [boosterForGame, setBoosterForGame] = useState(0);
    const [timeForGame, setTimeForGame] = useState(0);
    function sendMessage (data){
        setTimeout(()=>{
            if(window.ReactNativeWebView){
                window.ReactNativeWebView.postMessage(JSON.stringify(data));
            }else if(window.parent){
                window.parent.postMessage(JSON.stringify(data), "*");
            }
        }, 100)
    }

    const onMessage = (event) => {
        const data = JSON.parse(event.data);
        if (data.type === 'ACTION_BACK') {
            setTimeout(()=>{
                if(window.ReactNativeWebView)
                     window.ReactNativeWebView.postMessage(JSON.stringify({ type: "ACTION_POP" }));
            }, 100)
        }else if(data.type === 'START'){
            setPayLoad(data.payLoad);
            setTimeout(()=>{
                window.location.href = 'game';
            }, 1000);
        }
    }
    const onMessageTwo = (event) => {
        const data = JSON.parse(event.data);
        if (data.type === 'ACTION_BACK') {
            setTimeout(()=>{
                if(window.ReactNativeWebView){
                    window.ReactNativeWebView.postMessage(JSON.stringify({ type: "ACTION_POP" }));
                }else if(window.parent){
                    window.parent.postMessage(JSON.stringify({ type: "ACTION_POP" }), "*");
                }
            }, 100)
        }else if(data.type === 'START'){
            setPayLoad(data.payLoad);
            setTimeout(()=>{
                window.location.href = 'game';
            }, 1000);
        }
    }
    useEffect(() => {
        window.addEventListener("message", onMessageTwo ,false);
        document.addEventListener("message", onMessage ,false);
        return () => {
            document.removeEventListener("message", onMessage ,false);
            window.removeEventListener("message", onMessageTwo ,false);
        }
    }, []);
    useEffect(() => {
        if(url === 'web'){
            sendMessage({type:"READY"});
        }else{
            const query = window.location.search;
            let data = query.split('user_id=')[1];
            
            if (cookies.get('token')) {
                getInfo((response) => {
                    if(response.status === 200){
                        setUserInfo(response.data);
                        setLoading(false);
                    }
                });
                getHints((response) => {
                    setBooster(response.data[0]);
                    setExtraTime(response.data[1]);
                    if(response.status === 200){
                        
                    }
                    
                });
            }
            else {
                getToken(data, (response) => {
                    if (response.detail === "succeed") {
                        cookies.set('token', response.token, {path: '/', expires: new Date(2022, 30, 12)});
                        getInfo((response) => {
                            if(response.status === 200){
                                setUserInfo(response.data);
                                setLoading(false);
                            }
                        });
                        getHints((response) => {
                            setBooster(response.data[0]);
                            setExtraTime(response.data[1]);
                            if(response.status === 200){
                                
                            }
                            
                        });
                    }
                });
            }
        }
    },[]);
    return (
        <React.Fragment>
            <MediaQuery minWidth={320} maxWidth={480}>
                <Suspense fallback={<div></div>}>
                    <MainPageXs
                        userInfo={userInfo}
                        loading={loading}
                        timeForGame={timeForGame}
                        boosterForGame={boosterForGame}
                        setBoosterForGame={setBoosterForGame}
                        setTimeForGame={setTimeForGame}
                        extraTime={extraTime}
                        booster={booster}
                        hintDetail={hintDetail}
                        loading={loading}
                        setHintDetail={setHintDetail}
                        setUserInfo={setUserInfo}
                        coinText={coinText}
                        setCoinText={setCoinText}
                    />
                </Suspense>
            </MediaQuery>
            <MediaQuery minWidth={481} maxWidth={768}>
                <Suspense fallback={<div></div>}>
                    <MainPageSm
                        userInfo={userInfo}
                        loading={loading}
                        timeForGame={timeForGame}
                        boosterForGame={boosterForGame}
                        setBoosterForGame={setBoosterForGame}
                        setTimeForGame={setTimeForGame}
                        extraTime={extraTime}
                        booster={booster}
                        hintDetail={hintDetail}
                        loading={loading}
                        setHintDetail={setHintDetail}
                        setUserInfo={setUserInfo}
                        coinText={coinText}
                        setCoinText={setCoinText}
                    />  
                </Suspense>
            </MediaQuery>    
            <MediaQuery minWidth={769}>
                <Suspense fallback={<div></div>}>
                    <MainPageXl
                        userInfo={userInfo}
                        loading={loading}
                        timeForGame={timeForGame}
                        boosterForGame={boosterForGame}
                        setBoosterForGame={setBoosterForGame}
                        setTimeForGame={setTimeForGame}
                        extraTime={extraTime}
                        booster={booster}
                        hintDetail={hintDetail}
                        loading={loading}
                        setHintDetail={setHintDetail}
                        setUserInfo={setUserInfo}
                        coinText={coinText}
                        setCoinText={setCoinText}
                    />
                </Suspense>
            </MediaQuery>
        </React.Fragment>
    );
}

export default MainPage;