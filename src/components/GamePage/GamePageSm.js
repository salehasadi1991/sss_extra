import React, {useState, useEffect} from 'react';
import './styles/GamePageSm.scss';
import {Link} from 'react-router-dom';
import persianJs from 'persianjs';

const GamePageSm = (props) => {
    return (
        <div className="game-page-outer-container-sm">
            <div className="game-page-inner-container-sm">
                <div className="main-game-parent">
                    {props.urlBase === 'web'?'':<div className="game-header">
                        <h1 className="timer">{persianJs(props.clock.toString()).englishNumber().toString()}</h1>
                        <h1 className="point">امتیاز:{persianJs(props.point.toString()).englishNumber().toString()}</h1>
                    </div>}
                    <div className="main-game">
                        <div className="game-page-question-box" id="rotate" >
                            {props.item.map((item,index, value) =>
                                <React.Fragment key ={index}> 
                                   <p 
                                        className="game-page-question-item" 
                                        style={{backgroundColor: item.bgcolor,visibility: item.visibility}}
                                        onClick={()=>{
                                            if(props.game === true){
                                                if(item.key){
                                                    props.setAnswer(answer=>[...props.answer,item.key]);
                                                    for(var i = 0; i < props.tableGame.length; i++) {
                                                        if(props.tableGame[i].key === item.key) {
                                                            props.tableGame[i].bgcolor ="#5176ec"
                                                        }
                                                    }
                                                }else{
                                                    if(props.point > 5 ){
                                                        props.setPoint((prevState) => (props.point -5));
                                                        props.sendMessage(
                                                            {
                                                                type:"ACTION_SCORE",
                                                                payload:{score:props.point - 5,type:'dec'}
                                                            }
                                                        );
                                                    }else{
                                                        props.setPoint(0);
                                                        props.sendMessage(
                                                            {
                                                                type:"ACTION_SCORE",
                                                                payload:{score:0,type:'dec'}
                                                            }
                                                        );
                                                    }
                                                    props.setFeedBack('✘');
                                                    props.setFeedBackColor('red');
                                                    setTimeout(function(){
                                                        props. setLevel(level => props.level + 1);
                                                        props.setFeedBack('');
                                                        props.setFeedBackColor('');
                                                        props.setAnswer('');
                                                        props.setGame(false);
                                                        document.getElementById('rotate').style.transform = 'rotate(0deg)';
                                                    }, 500);
                                                }
                                            }
                                        }}
                                    >
                                        {item.data}
                                    </p>
                                </React.Fragment> 
                            )}
                        </div>
                        <div className="fidback">
                            <p style={{color:props.feedBackColor}}>{props.feedBack}</p>
                        </div>
                        <div className="game-page-question-box">
                            {props.withOutItem.map((item,index, value) =>
                                <React.Fragment key ={index}> 
                                    <p className="game-page-question-item" style={{backgroundColor: item.bgcolor}}>{item.data}</p>
                                </React.Fragment> 
                            )}
                        </div>
                    </div>
                </div>
                <div className="game-page-start-click">
                    {props.clickble ===true?
                    <p className="click-to-show-answers" onClick={()=>{

                        let removeitem = props.item[Math.floor(Math.random()*props.item.length)];
                        props.setRemoveItem(removeitem);

                        let withoutitem=[]
                        for(let i = 0 ;i < props.item.length ; i++){
                            if (removeitem !== props.item[i]) {
                                withoutitem.push(props.item[i]);
                            } 
                        }

                        props.setWithOutItem(withoutitem);
                        props.setItem([]);
                        props.setClickble(false);

                    }}></p>
                    :
                    <div className="game-page-question-box">
                        {props.answer.map((item,index, value) =>
                            <React.Fragment key ={index}> 
                                <p 
                                    className="game-page-question-item answers" 
                                    style={{backgroundColor: item.bgcolor}}
                                    onClick={() => props.setClickAnswer(item.key)}
                                >
                                    {item.data}
                                </p>
                            </React.Fragment> 
                        )}
                    </div>
                    }
                    <input 
                        type="button" 
                        className="button_acc"
                        value="به خاطر سپردم" 
                        onClick={()=>{
                            for(var i = 0; i < props.item.length; i++) {
                                for(var i = 0; i < props.tableGame.length; i++) {
                                    if(props.tableGame[i].key === props.item[i].key) {
                                        props.tableGame[i].bgcolor ="#fff"
                                    }
                                }
                            } 
                            setTimeout(function(){
                                document.getElementById('rotate').style.transform = 'rotate('+props.degree+'deg)';
                                props.setGame(true);
                            }, 700);

                            
                        }} 
                    />
                </div>
                <label htmlFor="game-page-end" id="auto"></label>
                <input className="game-page-end-state" id="game-page-end" type="checkbox" />
                <div className="game-page-end">
                    <div className="game-page-end-inner">
                        <div className="modal-box-for">
                            <div className="modal-box-cent">
                                <h1 className="modal-point"> امتیاز شما تو این بازی <span>{persianJs(props.point.toString()).englishNumber().toString()}</span></h1>
                            </div>
                            <div className="modal-box-cent">
                                <h1 className="modal-point"> رکورد قبلی <span>{props.userInfo.max_score ? persianJs(props.userInfo.max_score.toString()).englishNumber().toString():""}</span></h1>
                            </div>
                            <Link to={'/'}>
                                <div className="modal-box-game-link-game">
                                    <p>صفحه اصلی</p>
                                </div>
                            </Link>
                        </div>
                    </div>
                </div>
                <div className="game-footer">
                    <h2>بلوک ها را به خاطر بسپار</h2>
                </div>
            </div>  
        </div>
    );
}
export default GamePageSm;