import axios from 'axios';
import {baseURL, cookies} from '../../../Global.js';


export  function setScore(userPoint,callback) {
    axios({
        url: baseURL + 'v1/gme/score/',
        method:'post',
        data:{
            "score": userPoint
        },
        headers: {
            'Authorization': 'TOKEN ' + cookies.get('token'), 
        },
    })
    .then(response => {
        callback(response);
    })
    .catch(error => {
        callback(error.response);
    });
}
export  function getInfo(callback) {
    axios({
        url: baseURL + 'v1/usr/info/',
        method:'get',
        headers: {
            'Authorization': 'TOKEN ' + cookies.get('token'), 
        },
    })
    .then(response => {
        callback(response);
    })
    .catch(error => {
        callback(error.response);
    });
}