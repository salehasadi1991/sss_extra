import React, {lazy, Suspense, useState, useEffect} from 'react';
import MediaQuery from 'react-responsive';
import {setScore, getInfo} from './network/game';
import {useInterval} from './utility/timer.js';

const GamePageXl = lazy(() => import('./GamePageXl'));
const GamePageXs = lazy(() => import('./GamePageXs'));
const GamePageSm = lazy(() => import('./GamePageSm'));
const urlBase = window.location.href.split('-')[1];

const GamePage = (props) => {
    const [point, setPoint] = useState(0);
    const [clock, setClock] = useState(60);
    const [level, setLevel] = useState(0);
    const [item, setItem] = useState([]);
    const [answer, setAnswer] = useState([]);
    const [game, setGame] = useState(false);
    const [clickble, setClickble] = useState(true);
    const [withOutItem , setWithOutItem]=useState([]);
    const [outherAnswer , setOutherAnswer]=useState([]);
    const [removeItem , setRemoveItem]=useState({});
    const [degree , setDrgree] = useState('');
    const [feedBack , setFeedBack] = useState('');
    const [feedBackColor , setFeedBackColor] = useState('');
    const [userInfo, setUserInfo] = useState([]);
    const [tableGame, setTableGame] = useState([]);
    const [booster, setBooster] = useState();
    const [extraTime, setExtraTime] = useState();
    const [gameMode, setGameMode] = useState('hard');
    const [payload , setPayload]= useState();
    function sendMessage (data){
        setTimeout(()=>{
            if(window.ReactNativeWebView){
                 window.ReactNativeWebView.postMessage(JSON.stringify(data));
            } else if(window.parent){
                window.parent.postMessage(JSON.stringify(data), "*");
            }
        }, 100)
    }
    const onMessage = (event) => {
        const data = JSON.parse(event.data);
        if (data.type === 'ACTION_BACK') {
            props.history.replace('/');
        }
        if (data.type === 'END') {
            setGame(false);
            if (data.type === 'START') {
                setPayload(data.payload);
                setPoint(data.payload.score);
                setGameMode(data.payload.mode);
            }
        }
        if (data.type === 'START') {
            setPayload(data.payload);
            setPoint(data.payload.score);
            setGameMode(data.payload.mode);
        }
    }
    const onMessageTwo = (event) => {
        const data = JSON.parse(event.data);
        if (data.type === 'ACTION_BACK') {
            props.history.replace('/');
        }
        if (data.type === 'END') {
            setGame(false);
            if (data.type === 'START') {
                setPayload(data.payload);
                setPoint(data.payload.score);
                setGameMode(data.payload.mode);
            }
        }
        if (data.type === 'START') {
            setPayload(data.payload);
            setPoint(data.payload.score);
            setGameMode(data.payload.mode);
        }
    }
    useEffect(() => {
        document.addEventListener("message", onMessage ,false);
        window.addEventListener("message", onMessageTwo ,false);
        return () => {
            document.removeEventListener("message", onMessage ,false);
            window.removeEventListener("message", onMessageTwo ,false);
        }
    }, []);
    useInterval(() => {
        if(urlBase === 'web'){
            setClock(clock +1);
        }else{
            setClock(clock -1);
            if(clock === 1){
                setGame(false);
                document.getElementById("auto").click();
                if(booster === 1){
                    setPoint((prevState) => (point *2));
                    setScore(point * 2, (response) => {});
                }else{
                    setScore(point, (response) => {});
                }
            }
        }
    }, clock ? 1000 : null);
    useEffect(()=>{
        if(urlBase === 'web'){}else{
            if(props.location.hints){
                setBooster(props.location.hints.booster);
                setExtraTime(props.location.hints.extra_time);
                if(props.location.hints.extra_time === 1){
                    setClock(90)
                }
            }
            getInfo((response) => {
                if(response.status === 200){
                    setUserInfo(response.data);
                }
            });
        }
    },[]);
    useEffect(() => {
        let data = [
            {key:1},
            {key:2},
            {key:3},
            {key:4},
            {key:5},
            {key:6},
            {key:7},
            {key:8},
            {key:9},
            {key:10},
            {key:11},
            {key:12},
            {key:13},
            {key:14},
            {key:15},
            {key:16},
        ];
        let tableGameArray =[
            {id:0,bgcolor:"#fff",visibility: "visible"},{id:1,bgcolor:"#fff",visibility: "visible"},{id:2,bgcolor:"#fff",visibility: "visible"},{id:3,bgcolor:"#fff",visibility: "visible"},
            {id:4,bgcolor:"#fff",visibility: "visible"},{id:5,bgcolor:"#fff",visibility: "visible"},{id:6,bgcolor:"#fff",visibility: "visible"},{id:7,bgcolor:"#fff",visibility: "visible"},
            {id:8,bgcolor:"#fff",visibility: "visible"},{id:9,bgcolor:"#fff",visibility: "visible"},{id:10,bgcolor:"#fff",visibility: "visible"},{id:11,bgcolor:"#fff",visibility: "visible"},
            {id:12,bgcolor:"#fff",visibility: "visible"},{id:13,bgcolor:"#fff",visibility: "visible"},{id:14,bgcolor:"#fff",visibility: "visible"},{id:15,bgcolor:"#fff",visibility: "visible"}
        ];
        const item = data.sort( () => Math.random() - 0.2);
        let outheranswer = item.slice(5,8);
        
        setOutherAnswer(outheranswer);
        if(urlBase === 'web'){
            if(gameMode=== 'easy'){
                let degrees = [90,-90];
                const deg = degrees[Math.floor(Math.random() * degrees.length)];
                setDrgree(deg);
                let sliceLevel =data.slice(13);
                let sliceIndex =tableGameArray.slice(10);
                tableGameArray.forEach((x, index) => Object.assign(x, sliceLevel[index]));
                let randomTableGame = tableGameArray.sort( () => Math.random() - 0.5);
                
                setTableGame(randomTableGame);
                setItem(randomTableGame);
                for(let i = 0; i < randomTableGame.length; i++) {
                    if (randomTableGame[i].key){
                        randomTableGame[i].visibility ="hidden"
                    }
                }
                for(let i = 0; i < randomTableGame.length; i++) {
                    if (randomTableGame[i].key){
                        randomTableGame[i].visibility ="visible"
                        randomTableGame[i].bgcolor ="#5176ec"
                    }
                }
            }else if(gameMode=== 'medium'){
                let degrees = [90,-90];
                const deg = degrees[Math.floor(Math.random() * degrees.length)];
                setDrgree(deg);
                let sliceLevel =data.slice(13);
                let sliceIndex =tableGameArray.slice(10);
                tableGameArray.forEach((x, index) => Object.assign(x, sliceLevel[index]));
                let randomTableGame = tableGameArray.sort( () => Math.random() - 0.5);
                
                setTableGame(randomTableGame);
                setItem(randomTableGame);
                for(let i = 0; i < randomTableGame.length; i++) {
                    if (randomTableGame[i].key){
                        randomTableGame[i].visibility ="hidden"
                    }
                }
                for(let i = 0; i < randomTableGame.length; i++) {
                    if (randomTableGame[i].key){
                        randomTableGame[i].visibility ="visible"
                        randomTableGame[i].bgcolor ="#5176ec"
                    }
                }
            }else if(gameMode=== 'hard'){
                let degrees = [90,-90];
                const deg = degrees[Math.floor(Math.random() * degrees.length)];
                setDrgree(deg);
                let sliceLevel =data.slice(13);
                let sliceIndex =tableGameArray.slice(10);
                tableGameArray.forEach((x, index) => Object.assign(x, sliceLevel[index]));
                let randomTableGame = tableGameArray.sort( () => Math.random() - 0.5);
                
                setTableGame(randomTableGame);
                setItem(randomTableGame);
                for(let i = 0; i < randomTableGame.length; i++) {
                    if (randomTableGame[i].key){
                        randomTableGame[i].visibility ="hidden"
                    }
                }
                for(let i = 0; i < randomTableGame.length; i++) {
                    if (randomTableGame[i].key){
                        randomTableGame[i].visibility ="visible"
                        randomTableGame[i].bgcolor ="#5176ec"
                    }
                }
            }
        }else{
            let degrees = [90,-90];
            const deg = degrees[Math.floor(Math.random() * degrees.length)];
            setDrgree(deg);
            let sliceLevel =data.slice(13);
            let sliceIndex =tableGameArray.slice(10);
            tableGameArray.forEach((x, index) => Object.assign(x, sliceLevel[index]));
            let randomTableGame = tableGameArray.sort( () => Math.random() - 0.5);
            
            setTableGame(randomTableGame);
            setItem(randomTableGame);
            for(let i = 0; i < randomTableGame.length; i++) {
                if (randomTableGame[i].key){
                    randomTableGame[i].visibility ="hidden"
                }
            }
            for(let i = 0; i < randomTableGame.length; i++) {
                if (randomTableGame[i].key){
                    randomTableGame[i].visibility ="visible"
                    randomTableGame[i].bgcolor ="#5176ec"
                }
            }
        }
    }, [level]);

    useEffect(()=>{
        if(game === true){

                if(answer.length === 3){
                    setPoint((prevState) => (point + 10));
                    sendMessage(
                        {
                            type:"ACTION_SCORE",
                            payload:{score:point +10,type:'inc'}
                        }
                    );
                    setFeedBack('✔');
                    setFeedBackColor('#04B404');
                    setWithOutItem([]);
                    setClickble(true);
                    setAnswer('');
                    setTimeout(function(){
                        setLevel(level => level + 1);
                        setFeedBack('');
                        setFeedBackColor('');
                        setAnswer('');
                        setGame(false);
                        document.getElementById('rotate').style.transform = 'rotate(0deg)';
                    }, 500);
                }
        }
    }, [answer])
    return (
        <React.Fragment>
            <MediaQuery minWidth={320} maxWidth={480}>
                <Suspense fallback={<div></div>}>
                    <GamePageXs 
                        setClock={setClock}
                        urlBase={urlBase}
                        clock={clock}
                        setGame={setGame}
                        game={game}
                        point={point}
                        setPoint={setPoint}
                        answer={answer}
                        item={item}
                        setItem={setItem}
                        clickble={clickble}
                        setClickble={setClickble}
                        setAnswer={setAnswer}
                        setWithOutItem={setWithOutItem}
                        withOutItem={withOutItem}
                        outherAnswer={outherAnswer}
                        setRemoveItem={setRemoveItem}
                        removeItem={removeItem}
                        userInfo={userInfo}
                        setLevel={setLevel}
                        level={level}
                        setTableGame={setTableGame}
                        tableGame={tableGame}
                        setFeedBack={setFeedBack}
                        feedBack={feedBack}
                        feedBackColor={feedBackColor}
                        setFeedBackColor={setFeedBackColor}
                        sendMessage={sendMessage}
                        degree={degree}
                    />
                </Suspense>
            </MediaQuery>
            <MediaQuery minWidth={481} maxWidth={768}>
                <Suspense fallback={<div></div>}>
                    <GamePageSm
                        setClock={setClock}
                        urlBase={urlBase}
                        clock={clock}
                        setGame={setGame}
                        game={game}
                        point={point}
                        setPoint={setPoint}
                        answer={answer}
                        item={item}
                        setItem={setItem}
                        clickble={clickble}
                        setClickble={setClickble}
                        setAnswer={setAnswer}
                        setWithOutItem={setWithOutItem}
                        withOutItem={withOutItem}
                        outherAnswer={outherAnswer}
                        setRemoveItem={setRemoveItem}
                        removeItem={removeItem}
                        userInfo={userInfo}
                        setLevel={setLevel}
                        level={level}
                        setTableGame={setTableGame}
                        tableGame={tableGame}
                        setFeedBack={setFeedBack}
                        feedBack={feedBack}
                        feedBackColor={feedBackColor}
                        setFeedBackColor={setFeedBackColor}
                        sendMessage={sendMessage}
                        degree={degree}
                    />  
                </Suspense>
            </MediaQuery>    
            <MediaQuery minWidth={769}>
                <Suspense fallback={<div></div>}>
                    <GamePageXl
                        setClock={setClock}
                        urlBase={urlBase}
                        clock={clock}
                        setGame={setGame}
                        game={game}
                        point={point}
                        setPoint={setPoint}
                        answer={answer}
                        item={item}
                        setItem={setItem}
                        clickble={clickble}
                        setClickble={setClickble}
                        setAnswer={setAnswer}
                        setWithOutItem={setWithOutItem}
                        withOutItem={withOutItem}
                        outherAnswer={outherAnswer}
                        setRemoveItem={setRemoveItem}
                        removeItem={removeItem}
                        userInfo={userInfo}
                        setLevel={setLevel}
                        level={level}
                        setTableGame={setTableGame}
                        tableGame={tableGame}
                        setFeedBack={setFeedBack}
                        feedBack={feedBack}
                        feedBackColor={feedBackColor}
                        setFeedBackColor={setFeedBackColor}
                        sendMessage={sendMessage}
                        degree={degree}
                     />
                </Suspense>
            </MediaQuery>
        </React.Fragment>
    );
}

export default GamePage;